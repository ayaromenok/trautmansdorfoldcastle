## Low Austria castles

[NO Burgen online](http://noeburgen.imareal.sbg.ac.at/result/burgid/451)

## Maps

 - [1780](https://mapire.eu/en/map/europe-18century-firstsurvey/?layers=here-aerial%2C163%2C165&bbox=1847385.0783475742%2C6108757.093977315%2C1856485.862027778%2C6111623.482538009)
 - [1810](https://mapire.eu/en/map/europe-19century-secondsurvey/?layers=here-aerial%2C158%2C164&bbox=1850147.764224488%2C6109812.788022962%2C1854698.1560645897%2C6111245.982303309)
 - [1870](https://mapire.eu/en/map/europe-19century-thirdsurvey/?layers=160%2C166&bbox=1850343.43716942%2C6109663.244972338%2C1854893.8290095218%2C6111096.439252685)
 - [Cadastral ~1820] (https://mapire.eu/en/map/cadastral/?layers=3%2C4&bbox=1851287.6574921992%2C6110097.637178452%2C1853562.8534122503%2C6110814.234318626)
